# Perform tests over the eventClose_keepAlive.feature
# Perform tests over the VLC_tests.feature


from behave import *
from my_functions_class import LiveStreamService
from my_functions_class import test_finished
import requests
from requests.auth import HTTPBasicAuth

mac = 'AA:BB:CC:DD:EE:56'
environment = 'ist'
username = 'mirgor'
password = 'wayna!'

my_LSS = LiveStreamService(mac, environment, username, password)


@then(u'Perform an eventClose')
def step_impl(context):
    print()
    req = my_LSS.perform_eventClose()
    print()

    # Asserts
    assert 200 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@then(u'Perform an eventClose without initSession')
def step_impl(context):
    print()
    req = my_LSS.perform_eventClose()

    # Asserts
    assert 404 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@when(u'Perform an eventClose with special characters')
def step_impl(context):
    print()
    req = requests.post('https://' + environment + '-vs.mirgor.com.ar/livestream/eventClose/' +
                        'AA%3ABB%3ACC%3ADD%3AEE%3A5%',
                        auth=HTTPBasicAuth(username, password))

    # Checking by HTTP Response
    print("Status: " + str(req.status_code))
    print("Content: " + str(req.content))
    print()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@when(u'Perform an eventClose with more than 12 digits')
def step_impl(context):
    print()
    print("Performing an eventClose - MAC with more than 12 digits")
    req = requests.post('https://' + environment + '-vs.mirgor.com.ar/livestream/eventClose/' +
                        'AA%3ABB%3ACC%3ADD%3AEE%3A53%312',
                        auth=HTTPBasicAuth(username, password))

    # Checking by HTTP Response
    print("Status: " + str(req.status_code))
    print("Content: " + str(req.content))
    print()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()

