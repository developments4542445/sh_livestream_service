import requests
import time
from requests.auth import HTTPBasicAuth


class LiveStreamService:
    req = 'request'     # for json elements
    uuid = 'uuid'
    host_used = 'host_used'
    port_used = 'port_used'
    video_username = 'video_username'
    video_password = 'video_password'

    def __init__(self, mac='AA:BB:CC:DD:EE:53', environment='ist', username='mirgor', password='wayna!'):
        self.mac = mac
        self.environment = environment
        self.username = username
        self.password = password

    def mac_string_change(self, string_mac):
        separator = '%3A'
        string_mac_sep = string_mac[0:2] + separator + string_mac[3:5] + separator + string_mac[6:8] + separator + \
                         string_mac[9:11] + separator + string_mac[12:14] + separator + string_mac[15:17]
        return string_mac_sep

    def perform_aliveMap(self):
        print("Performing an aliveMap")
        mac_changed = self.mac_string_change(self.mac)
        req = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/livestream/private/aliveMap/' +
                           mac_changed,
                           auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))
        print('uuId aliveMap: ' + self.uuid)

        return req

    def perform_endSession(self):
        print("Performing an endSession")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/endSession',
                            json={'cameraMac': self.mac, 'uuId': self.uuid},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))
        print('uuId endSession: ' + self.uuid)

        return req

    def perform_eventClose(self):
        print("Performing an eventClose")
        mac_changed = self.mac_string_change(self.mac)
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/eventClose/' +
                            mac_changed,
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    def perform_keepAlive(self):
        print("Performing a keepAlive")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/keepAlive/',
                            json={'cameraMac': self.mac, 'uuId': self.uuid},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    def perform_initSession(self):
        print("Performing an initSession")
        self.req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/initSession',
                                 json={'cameraMac': self.mac, 'protocol': 'RTSP'},
                                 auth=HTTPBasicAuth(self.username, self.password))

        req_in = self.req

        if req_in.status_code == 200:
            result = req_in.json()
            self.uuid = result['uuId']
            self.host_used = result['host']
            self.port_used = result['port']
            self.video_username = result['proxyServer']['username']
            self.video_password = result['proxyServer']['password']

        # Checking by HTTP Response
        print("Status: " + str(req_in.status_code))
        print("Content: " + str(req_in.content))
        print('uuId: ' + self.uuid)

        return req_in

    def perform_portTCPInUse(self):
        print("Performing a portTCPInUse")
        req = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/livestream/private/portTCPInUse/' +
                           str(self.port_used),
                           auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))
        print('port: ' + str(self.port_used))

        return req

    def perform_statistics_specs(self):
        print("Performing a statistics/specs")
        req = requests.get('https://' + self.environment +
                           '-vs.mirgor.com.ar/livestream/statistics/specs?page=0&size=100&sort=id,'
                           'desc&search=(cameraMac:%27' + self.mac + '%27)',
                           auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))

        return req


def test_finished():
    print()
    print('Last print do not work properly')
    pass
