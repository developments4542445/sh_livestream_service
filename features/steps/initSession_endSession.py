# Perform tests over the eventClose_keepAlive.feature
# Perform tests over the initSession_endSession.feature
# Perform tests over the internalApi.feature
# Perform tests over the VLC_tests.feature

from behave import *
from my_functions_class import LiveStreamService
from my_functions_class import test_finished
import time
import requests
import vlc
from requests.auth import HTTPBasicAuth

mac = 'AA:BB:CC:DD:EE:56'
environment = 'ist'
username = 'mirgor'
password = 'wayna!'

my_LSS = LiveStreamService(mac, environment, username, password)
my_LSS_2 = LiveStreamService(mac, environment, username, password)
my_LSS_3 = LiveStreamService(mac, environment, username, password)


@given(u'Perform an initSession')
def step_impl(context):
    print()
    req = my_LSS.perform_initSession()

    # Asserts
    assert 200 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@given(u'Wait to avoid 429 message')
def step_impl(context):
    time.sleep(4)  # Delay to avoid 429 - incorrect answer
    print()
    print('Wait successfully done')

    test_finished()


@then(u'Wait to avoid 429 message')
def step_impl(context):
    time.sleep(4)  # Delay to avoid 429 - incorrect answer
    print()
    print('Wait successfully done')

    test_finished()


@when(u'Wait to avoid 429 message')
def step_impl(context):
    time.sleep(4)  # Delay to avoid 429 - incorrect answer
    print()
    print('Wait successfully done')

    test_finished()


@then(u'Perform an endSession')
def step_impl(context):
    print()
    req = my_LSS.perform_endSession()

    # Asserts
    assert 200 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@given(u'Finalize with eventClose and avoid 429 message')
def step_impl(context):
    print()
    req = my_LSS.perform_eventClose()
    time.sleep(4)
    print()

    test_finished()


@given(u'Perform 3 initSession')
def step_impl(context):
    print()
    req = my_LSS.perform_initSession()
    time.sleep(4)
    req2 = my_LSS_2.perform_initSession()
    time.sleep(4)
    req3 = my_LSS_3.perform_initSession()

    # Asserts
    assert 200 == req.status_code, "ERROR in first initSession. status_code: " + str(req.status_code) +\
                                   '. content: ' + str(req.content)
    assert 200 == req2.status_code, "ERROR in second initSession. status_code: " + str(req.status_code) +\
                                    '. content: ' + str(req.content)
    assert 200 == req3.status_code, "ERROR in third initSession. status_code: " + str(req.status_code) +\
                                    '. content: ' + str(req.content)

    test_finished()


@then(u'Perform 3 endSession')
def step_impl(context):
    print()
    req = my_LSS.perform_endSession()
    time.sleep(4)
    req2 = my_LSS_2.perform_endSession()
    time.sleep(4)
    req3 = my_LSS_3.perform_endSession()

    # Asserts
    assert 200 == req.status_code, "ERROR in first initSession. status_code: " + str(req.status_code) +\
                                   '. content: ' + str(req.content)
    assert 200 == req2.status_code, "ERROR in second initSession. status_code: " + str(req.status_code) +\
                                    '. content: ' + str(req.content)
    assert 200 == req3.status_code, "ERROR in third initSession. status_code: " + str(req.status_code) +\
                                    '. content: ' + str(req.content)

    test_finished()


@When(u'Perform an initSession without cameraMac')
def step_impl(context):
    print()
    print("Performing an initSession without cameraMac")
    req = requests.post('https://' + environment + '-vs.mirgor.com.ar/livestream/initSession',
                        json={'cameraMac': mac},
                        auth=HTTPBasicAuth(username, password))

    # Checking by HTTP Response
    print("Status: " + str(req.status_code))
    print("Content: " + str(req.content))
    print()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@when(u'Perform an initSession without protocol')
def step_impl(context):
    print()
    print("Performing an initSession without protocol")
    req = requests.post('https://' + environment + '-vs.mirgor.com.ar/livestream/initSession',
                        json={'protocol': 'RTSP'},
                        auth=HTTPBasicAuth(username, password))

    # Checking by HTTP Response
    print("Status: " + str(req.status_code))
    print("Content: " + str(req.content))
    print()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@when(u'Perform an initSession with special characters')
def step_impl(context):
    print()
    print("Performing an initSession - MAC with special characters")
    req = requests.post('https://' + environment + '-vs.mirgor.com.ar/livestream/initSession',
                        json={'cameraMac': 'AA:BB:CC:DD:EE:5&', 'protocol': 'RTSP'},
                        auth=HTTPBasicAuth(username, password))

    # Checking by HTTP Response
    print("Status: " + str(req.status_code))
    print("Content: " + str(req.content))
    print()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@when(u'Perform an initSession with more than 12 digits')
def step_impl(context):
    print()
    print("Performing an initSession - MAC with more than 12 digits")
    req = requests.post('https://' + environment + '-vs.mirgor.com.ar/livestream/initSession',
                        json={'cameraMac': 'AA:BB:CC:DD:EE:53:12', 'protocol': 'RTSP'},
                        auth=HTTPBasicAuth(username, password))

    # Checking by HTTP Response
    print("Status: " + str(req.status_code))
    print("Content: " + str(req.content))
    print()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@given(u'Perform an endSession with special characters')
def step_impl(context):
    print()
    print("Performing an endSession - MAC with special characters")
    req = requests.post('https://' + environment + '-vs.mirgor.com.ar/livestream/endSession',
                        json={'cameraMac': 'AA:BB:CC:DD:EE:5&', 'uuId': my_LSS.uuid},
                        auth=HTTPBasicAuth(username, password))

    # Checking by HTTP Response
    print("Status: " + str(req.status_code))
    print("Content: " + str(req.content))
    print()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@given(u'Perform an endSession with more than 12 digits')
def step_impl(context):
    print()
    print("Performing an endSession - MAC with more than 12 digits")
    req = requests.post('https://' + environment + '-vs.mirgor.com.ar/livestream/endSession',
                        json={'cameraMac': 'AA:BB:CC:DD:EE:53:12', 'uuId': my_LSS.uuid},
                        auth=HTTPBasicAuth(username, password))

    # Checking by HTTP Response
    print("Status: " + str(req.status_code))
    print("Content: " + str(req.content))
    print()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@given(u'Perform an endSession without uuid')
def step_impl(context):
    print()
    print("Performing an endSession - non valid uuId")
    req = requests.post('https://' + environment + '-vs.mirgor.com.ar/livestream/endSession',
                        json={'cameraMac': mac, 'uuId': 'uuId_non_valid'},
                        auth=HTTPBasicAuth(username, password))

    # Checking by HTTP Response
    print("Status: " + str(req.status_code))
    print("Content: " + str(req.content))
    print()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@then(u'Perform an initSession checking username and password')
def step_impl(context):
    print()
    req = my_LSS_2.perform_initSession()

    session_username_1 = my_LSS.req.json()['proxyServer']['username']
    session_password_1 = my_LSS.req.json()['proxyServer']['password']
    print('username 1: ' + session_username_1)
    print('password 1: ' + session_password_1)
    session_username_2 = my_LSS_2.req.json()['proxyServer']['username']
    session_password_2 = my_LSS_2.req.json()['proxyServer']['password']
    print('username 2: ' + session_username_2)
    print('password 2: ' + session_password_2)
    print()

    # Asserts
    assert 200 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    assert session_username_1 == session_username_2, "username is not the same in both situations"
    assert session_password_1 != session_password_2, "password is the same in both situations"

    test_finished()


@then(u'Perform an endSession after checking username and password')
def step_impl(context):
    print()
    req = my_LSS_2.perform_endSession()

    # Asserts
    assert 200 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@then(u'Check 3 username and passwords')
def step_impl(context):
    print()

    session_username_1 = my_LSS.req.json()['proxyServer']['username']
    session_password_1 = my_LSS.req.json()['proxyServer']['password']
    print('username 1: ' + session_username_1)
    print('password 1: ' + session_password_1)
    session_username_2 = my_LSS_2.req.json()['proxyServer']['username']
    session_password_2 = my_LSS_2.req.json()['proxyServer']['password']
    print('username 2: ' + session_username_2)
    print('password 2: ' + session_password_2)
    session_username_3 = my_LSS_3.req.json()['proxyServer']['username']
    session_password_3 = my_LSS_3.req.json()['proxyServer']['password']
    print('username 3: ' + session_username_3)
    print('password 3: ' + session_password_3)
    print()

    # Asserts
    assert session_username_1 == session_username_1 == session_username_3, \
        "username is not the same in every situations"
    assert session_password_1 == session_password_2 == session_password_3, \
        "password is not the same in every situations"

    test_finished()


@then(u'Check 429 message')
def step_impl(context):
    print()
    req2 = my_LSS_2.perform_initSession()

    # Asserts
    assert 429 == req2.status_code, "ERROR. status_code: " + str(req2.status_code) + '. content: ' + str(req2.content)

    test_finished()


@given(u'Perform an aliveMap without initSession generated')
def step_impl(context):
    print()
    req = my_LSS.perform_aliveMap()
    result = req.json()

    # Asserts
    assert 200 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)
    assert 0 == len(result), "There is an initSession launched"

    test_finished()


@then(u'Perform an aliveMap')
def step_impl(context):
    print()
    req = my_LSS.perform_aliveMap()
    result = req.json()

    # Asserts
    assert 200 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)
    assert 1 == len(result), "There is not an only initSession launched"

    test_finished()


@when(u'Perform an aliveMap with special characters')
def step_impl(context):
    print()
    print("Performing an aliveMap - MAC with special characters")
    req = requests.get('https://' + environment + '-vs.mirgor.com.ar/livestream/private/aliveMap/' +
                       'AA%3ABB%3ACC%3ADD%3AEE%3A5&',
                       auth=HTTPBasicAuth(username, password))

    # Checking by HTTP Response
    print("Status: " + str(req.status_code))
    print("Content: " + str(req.content))
    print()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@when(u'Perform an aliveMap with more than 12 digits')
def step_impl(context):
    print()
    print("Performing an aliveMap - MAC with more than 12 digits")
    req = requests.get('https://' + environment + '-vs.mirgor.com.ar/livestream/private/aliveMap/' +
                       'AA%3ABB%3ACC%3ADD%3AEE%3A53%3A12',
                       auth=HTTPBasicAuth(username, password))

    # Checking by HTTP Response
    print("Status: " + str(req.status_code))
    print("Content: " + str(req.content))
    print()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@when(u'Perform a portTCPInUse')
def step_impl(context):
    print()
    req = my_LSS.perform_portTCPInUse()

    # Asserts
    assert 200 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)
    assert my_LSS.port_used != 'port_used', "port is not indicated as used: " + my_LSS.port_used

    test_finished()


@when(u'Perform a portTCPInUse - initSession not generated')
def step_impl(context):
    print()
    req = my_LSS.perform_portTCPInUse()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)
    assert my_LSS.port_used == 'port_used', "port is different than default value: " + my_LSS.port_used

    test_finished()


@then(u'Perform aliveMap, keepAlive and comparison of expiration data')
def step_impl(context):
    print()
    req = my_LSS.perform_aliveMap()
    result = req.json()
    time_1 = result[0]['expiration']
    print('expiration: ' + time_1)
    print()

    # Asserts
    assert 200 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)
    assert 1 == len(result), "There is not an initSession launched"

    # --------------------------------------------------------------------------------------------------------------

    time.sleep(4)  # Delay to avoid 429 - incorrect answer

    # --------------------------------------------------------------------------------------------------------------

    req2 = my_LSS.perform_keepAlive()
    print()

    # Asserts
    assert 200 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    # --------------------------------------------------------------------------------------------------------------

    time.sleep(4)  # Delay to avoid 429 - incorrect answer

    # --------------------------------------------------------------------------------------------------------------

    req4 = my_LSS.perform_aliveMap()
    result4 = req4.json()
    time_2 = result4[0]['expiration']
    print('expiration: ' + time_2)
    print()

    # Asserts
    assert 200 == req4.status_code, "ERROR. status_code: " + str(req4.status_code) + '. content: ' + str(req4.content)
    assert 1 == len(result4), "There is not an initSession launched"

    # --------------------------------------------------------------------------------------------------------------

    # Comparing time_1 and time_2
    pattern = '%d-%m-%Y %H:%M:%S'
    date_1 = int(time.mktime(time.strptime(time_1, pattern)))
    date_2 = int(time.mktime(time.strptime(time_2, pattern)))

    assert date_2 > date_1, 'After keepAlive, time is not updated.'

    test_finished()


@when(u'Perform a keepAlive with special characters')
def step_impl(context):
    print()
    print("Performing a keepAlive with special characters")
    req2 = requests.post('https://' + my_LSS.environment + '-vs.mirgor.com.ar/livestream/keepAlive/',
                         json={'cameraMac': 'AA:BB:CC:DD:EE:5&', 'uuId': my_LSS.uuid},
                         auth=HTTPBasicAuth(my_LSS.username, my_LSS.password))

    # Checking by HTTP Response
    print("Status: " + str(req2.status_code))
    print("Content: " + str(req2.content))
    print()

    # Asserts
    assert 400 == req2.status_code, "ERROR. status_code: " + str(req2.status_code) + '. content: ' + str(req2.content)

    test_finished()


@when(u'Perform a keepAlive with more than 12 digits')
def step_impl(context):
    print()
    print("Performing a keepAlive with more than 12 digits")
    req2 = requests.post('https://' + my_LSS.environment + '-vs.mirgor.com.ar/livestream/keepAlive/',
                         json={'cameraMac': 'AA:BB:CC:DD:EE:53:12', 'uuId': my_LSS.uuid},
                         auth=HTTPBasicAuth(my_LSS.username, my_LSS.password))

    # Checking by HTTP Response
    print("Status: " + str(req2.status_code))
    print("Content: " + str(req2.content))
    print()

    # Asserts
    assert 400 == req2.status_code, "ERROR. status_code: " + str(req2.status_code) + '. content: ' + str(req2.content)

    test_finished()


@when(u'Perform a keepAlive with no session started')
def step_impl(context):
    print()
    print("Performing a keepAlive with no session started")
    req = my_LSS.perform_keepAlive()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@when(u'Perform a keepAlive with non existent uuid')
def step_impl(context):
    print()
    print("Performing a keepAlive with non existent uuid")
    req2 = requests.post('https://' + my_LSS.environment + '-vs.mirgor.com.ar/livestream/keepAlive/',
                         json={'cameraMac': my_LSS.mac, 'uuId': 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa'},
                         auth=HTTPBasicAuth(my_LSS.username, my_LSS.password))

    # Checking by HTTP Response
    print("Status: " + str(req2.status_code))
    print("Content: " + str(req2.content))
    print()

    # Asserts
    assert 400 == req2.status_code, "ERROR. status_code: " + str(req2.status_code) + '. content: ' + str(req2.content)

    test_finished()


@when(u'Perform a keepAlive after endSession')
def step_impl(context):
    print()
    req = my_LSS.perform_keepAlive()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@when(u'Perform a keepAlive after eventClose')
def step_impl(context):
    print()
    req = my_LSS.perform_keepAlive()

    # Asserts
    assert 400 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)

    test_finished()


@when(u'Perform a keepAlive after session expired')
def step_impl(context):
    print()

    # Obtain if this was expired or not
    print('Checking sessions (update every 30 seconds).')
    expired = False
    while expired is False:
        time.sleep(30)  # Delay to avoid 429 - incorrect answer
        req2 = my_LSS.perform_statistics_specs()
        result2 = req2.json()
        element_event_close = result2['content'][0]['eventClose']
        print('eventClose: ' + str(element_event_close))
        print()

        # Asserts
        assert 200 == req2.status_code, "ERROR. status_code: " + str(req2.status_code) + \
                                        '. content: ' + str(req2.content)

        if str(element_event_close) == 'TIMEOUT':
            expired = True

    # --------------------------------------------------------------------------------------------------------------

    # At this point, the session has expired for a TIMEOUT

    time.sleep(4)  # Delay to avoid 429 - incorrect answer

    # --------------------------------------------------------------------------------------------------------------

    req3 = my_LSS_3.perform_keepAlive()
    print()

    # Asserts
    assert 400 == req3.status_code, "ERROR. status_code: " + str(req3.status_code) + '. content: ' + str(req3.content)

    test_finished()


@then(u'Check statistics to confirm an only session is opened')
def step_impl(context):
    print()
    req = my_LSS.perform_statistics_specs()
    result = req.json()
    element_max_session_concurrent = result['content'][0]['maxSessionConcurrent']
    print('maxSessionConcurrent: ' + str(element_max_session_concurrent))
    print()

    # Asserts
    assert 200 == req.status_code, "ERROR. status_code: " + str(req.status_code) + '. content: ' + str(req.content)
    assert str(element_max_session_concurrent) == '1', "maxSessionConcurrent is not 1"

    test_finished()


@then(u'Perform audio and video over VLC')
def step_impl(context):
    print()

    # Play VLC and confirm functionality
    try:
        print('Playing VLC to confirm functionality. Wait 10 seconds to open VLC.')
        mediaplayer = vlc.Instance("--rtsp-tcp")
        player = mediaplayer.media_player_new()
        media = mediaplayer.media_new('rtsp://' + my_LSS.video_username + ':' + my_LSS.video_password +
                                      '@' + my_LSS.host_used + ':' + str(my_LSS.port_used) + '/cam')
        print('rtsp://' + my_LSS.video_username + ':' + my_LSS.video_password +
              '@' + my_LSS.host_used + ':' + str(my_LSS.port_used) + '/cam')
        player.set_media(media)
        player.play()
        flag_vlc_error = False
    except:
        print("Error over the VLC streaming.")
        flag_vlc_error = True

    # Asserts
    assert flag_vlc_error is False, "Streaming could not be performed. Error was performed (not from VLC state)."

    # --------------------------------------------------------------------------------------------------------------

    # While video is loading, it shall wait
    player_state = player.get_state()
    while str(player_state) != 'State.Playing':
        player_state = player.get_state()

    time.sleep(2)
    audio_video_test = 'input'
    print()
    print("Please, confirm that audio and video are in this streaming."
          + " If not, there is a bug and it shall be loaded in Jira.")
    while audio_video_test != 'yes' and audio_video_test != 'audio' and audio_video_test != 'video':
        audio_video_test = input('Audio and video test success? What failed? [yes - audio - video]:')

    if audio_video_test == 'audio':
        player.stop()
        assert False, 'Performing audio over VLC does not success. Audio failure according to the user.'

    if audio_video_test == 'video':
        player.stop()
        assert False, 'Performing video over VLC does not success. Video failure according to the user.'

    player.stop()
    test_finished()


@then(u'Play VLC with wrong username')
def step_impl(context):
    print()
    # Play VLC and confirm functionality - wrong username
    try:
        print('Playing VLC WITH WRONG USERNAME to confirm functionality.')
        mediaplayer = vlc.Instance("--rtsp-tcp")
        player = mediaplayer.media_player_new()
        media = mediaplayer.media_new('rtsp://' + 'carlitos' + ':' + my_LSS.video_password +
                                      '@' + my_LSS.host_used + ':' + str(my_LSS.port_used) + '/cam')
        player.set_media(media)
        player.play()
        flag_vlc_error = False
    except:
        print("Error over the VLC streaming.")
        flag_vlc_error = True

    # Asserts
    assert flag_vlc_error is False, "Streaming could not be performed. Error was performed (not from VLC state)."

    # --------------------------------------------------------------------------------------------------------------

    # Checking if failed or not
    print('Checking if VLC is opened or not.')
    time.sleep(15)
    player_state = player.get_state()  # Get state of the VLC process

    # --------------------------------------------------------------------------------------------------------------

    # endSession if there was a streaming performed
    if str(player_state) == 'State.Playing':
        req2 = my_LSS.perform_endSession()
        print()

        # Asserts
        player.stop()
        assert 200 == req2.status_code, "ERROR. status_code: " + str(req2.status_code) + \
                                        '. content: ' + str(req2.content)
        assert 'State.Opening' == str(player_state), 'Streaming success... but it should indicate wrong username.'

    player.stop()
    test_finished()


@then(u'Play VLC with wrong password')
def step_impl(context):
    print()
    # Play VLC and confirm functionality - wrong username
    try:
        print('Playing VLC WITH WRONG PASSWORD to confirm functionality.')
        mediaplayer = vlc.Instance("--rtsp-tcp")
        player = mediaplayer.media_player_new()
        media = mediaplayer.media_new('rtsp://' + my_LSS.video_username + ':' + 'carlos_password' +
                                      '@' + my_LSS.host_used + ':' + str(my_LSS.port_used) + '/cam')
        player.set_media(media)
        player.play()
        flag_vlc_error = False
    except:
        print("Error over the VLC streaming.")
        flag_vlc_error = True

    # Asserts
    assert flag_vlc_error is False, "Streaming could not be performed. Error was performed (not from VLC state)."

    # --------------------------------------------------------------------------------------------------------------

    # Checking if failed or not
    print('Checking if VLC is opened or not.')
    time.sleep(15)
    player_state = player.get_state()  # Get state of the VLC process

    # --------------------------------------------------------------------------------------------------------------

    # endSession if there was a streaming performed
    if str(player_state) == 'State.Playing':
        req2 = my_LSS.perform_endSession()
        print()

        # Asserts
        player.stop()
        assert 200 == req2.status_code, "ERROR. status_code: " + str(req2.status_code) + \
                                        '. content: ' + str(req2.content)
        assert 'State.Opening' == str(player_state), 'Streaming success... but it should indicate wrong password.'

    player.stop()
    test_finished()


@then(u'eventClose to prevent previous initSessions')
def step_impl(context):
    print()
    time.sleep(4)  # Delay to avoid 429 - incorrect answer
    my_LSS.perform_eventClose()
    time.sleep(4)  # Delay to avoid 429 - incorrect answer
    print()

    test_finished()
