# To run this, we have to write:
# behave features/internalApi.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Took: 42.500 seconds
@every_scenario
Feature: LiveStream Service initSession and endSession

  @automation @finished @happy
  Scenario: aliveMap - Correct Answer
    Given Perform an aliveMap without initSession generated
    And Wait to avoid 429 message

  @automation @finished @happy
  Scenario: aliveMap - correct answer - initSession generated
    Given Finalize with eventClose and avoid 429 message
    Given Perform an initSession
    And Wait to avoid 429 message
    Then Perform an aliveMap
    And Wait to avoid 429 message
    Then Perform an endSession
    And Wait to avoid 429 message

  @automation @finished @rainy
  Scenario: aliveMap with mac wrong information
    When Perform an aliveMap with special characters
    Given Wait to avoid 429 message
    When Perform an aliveMap with more than 12 digits
    And Wait to avoid 429 message

  @automation @finished @happy
  Scenario: portTCPInUse - initSession generated
    Given Finalize with eventClose and avoid 429 message
    Given Perform an initSession
    And Wait to avoid 429 message
    When Perform a portTCPInUse
    And Wait to avoid 429 message
    Then Perform an endSession
    And Wait to avoid 429 message

  @automation @finished @rainy
  Scenario: portTCPInUse - initSession not generated
    When Perform a portTCPInUse - initSession not generated
    And Wait to avoid 429 message
