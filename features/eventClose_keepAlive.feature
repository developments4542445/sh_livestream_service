# To run this, we have to write:
# behave features/eventClose_keepAlive.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Took: 7 minutes 18.840 seconds
@every_scenario
Feature: LiveStream Service eventClose and keepAlive

  @automation @finished @rainy
  Scenario: End session with eventClose
    Given Perform an initSession
    And Wait to avoid 429 message
    Then Perform an eventClose
    And Wait to avoid 429 message

  @automation @finished @rainy
  Scenario: Logout of an already closed session
    Then Perform an eventClose without initSession
    And Wait to avoid 429 message

  @automation @finished @rainy
  Scenario: eventClose with mac wrong information
    When Perform an eventClose with special characters
    Given Wait to avoid 429 message
    When Perform an eventClose with more than 12 digits
    And Wait to avoid 429 message

  @automation @finished @happy
  Scenario: Update the expiration of a session
    Given Finalize with eventClose and avoid 429 message
    Given Perform an initSession
    And Wait to avoid 429 message
    Then Perform aliveMap, keepAlive and comparison of expiration data
    And Wait to avoid 429 message
    And Perform an endSession
    And Wait to avoid 429 message

  @automation @finished @rainy
  Scenario: keepAlive with mac wrong information
    Given Finalize with eventClose and avoid 429 message
    Given Perform an initSession
    And Wait to avoid 429 message
    When Perform a keepAlive with special characters
    Given Wait to avoid 429 message
    When Perform a keepAlive with more than 12 digits
    And Wait to avoid 429 message
    When Perform a keepAlive with non existent uuid
    And Wait to avoid 429 message
    Then Perform an endSession
    And Wait to avoid 429 message
    When Perform a keepAlive with no session started
    And Wait to avoid 429 message

  @automation @finished @rainy
  Scenario: update session expiration when session ended by endSession
    Given Finalize with eventClose and avoid 429 message
    Given Perform an initSession
    And Wait to avoid 429 message
    Then Perform an endSession
    Given Wait to avoid 429 message
    When Perform a keepAlive after endSession

  @automation @finished @rainy
  Scenario: update session expiration when session ended by eventClose
    Given Perform an initSession
    And Wait to avoid 429 message
    Then Perform an eventClose
    Given Wait to avoid 429 message
    When Perform a keepAlive after eventClose

  @automation @finished @rainy
  Scenario: update session expiration when session has expired
    Given Finalize with eventClose and avoid 429 message
    Given Perform an initSession
    And Wait to avoid 429 message
    When Perform a keepAlive after session expired

  @automation @finished @happy
  Scenario: initSession after an expired session
    Given Finalize with eventClose and avoid 429 message
    Given Perform an initSession
    When Perform a keepAlive after session expired
    And Wait to avoid 429 message
    Given Perform an initSession
    And Wait to avoid 429 message
    Then Check statistics to confirm an only session is opened
    And Wait to avoid 429 message
    And Perform an endSession
    And Wait to avoid 429 message
