# To run this, we have to write:
# behave features/VLC_tests.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Took: 1 minutes 5.775 seconds
@every_scenario
Feature: LiveStream Service VLC tests

  @semi_automation @finished @happy
  Scenario: InitSession and reproduce on VLC - Test username and password (success)
    Then eventClose to prevent previous initSessions
    And Wait to avoid 429 message
    Given Perform an initSession
    Then Perform audio and video over VLC
    And Perform an endSession
    And Wait to avoid 429 message

  @automation @finished @rainy
  Scenario: Enter username and/or password incorrectly
    Then eventClose to prevent previous initSessions
    And Wait to avoid 429 message
    Given Perform an initSession
    Then Play VLC with wrong username
    And Play VLC with wrong password
    And Perform an endSession
