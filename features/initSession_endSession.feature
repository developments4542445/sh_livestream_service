# To run this, we have to write:
# behave features/initSession_endSession.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Took: 3 minutes 31.560 seconds
@every_scenario
Feature: LiveStream Service initSession and endSession

  @automation @finished @happy
  Scenario: Disconnect an Only Device
    Given Finalize with eventClose and avoid 429 message
    Given Perform an initSession
    And Wait to avoid 429 message
    Then Perform an endSession
    And Wait to avoid 429 message

  @automation @finished @happy
  Scenario: Init more than one session and end all sessions
    Given Finalize with eventClose and avoid 429 message
    Given Perform 3 initSession
    And Wait to avoid 429 message
    Then Perform 3 endSession
    And Wait to avoid 429 message

  @automation @finished @happy
  Scenario: Init more than one session, end one session and init others session more
    Given Finalize with eventClose and avoid 429 message
    Given Perform 3 initSession
    And Wait to avoid 429 message
    Then Perform an endSession
    Given Wait to avoid 429 message
    And Perform an initSession
    And Wait to avoid 429 message
    Then Perform 3 endSession
    And Wait to avoid 429 message

  @automation @finished @rainy
  Scenario: Init session with empty required field
    Given Finalize with eventClose and avoid 429 message
    When Perform an initSession without cameraMac
    Given Wait to avoid 429 message
    When Perform an initSession without protocol
    And Wait to avoid 429 message

  @automation @finished @rainy
  Scenario: Init session with mac wrong information
    Given Finalize with eventClose and avoid 429 message
    When Perform an initSession with special characters
    Given Wait to avoid 429 message
    When Perform an initSession with more than 12 digits
    And Wait to avoid 429 message

  @automation @finished @rainy
  Scenario: End session with mac wrong information
    Given Finalize with eventClose and avoid 429 message
    Given Perform an initSession
    And Wait to avoid 429 message
    And Perform an endSession with special characters
    And Wait to avoid 429 message
    And Perform an endSession with more than 12 digits
    And Wait to avoid 429 message
    Then Perform an endSession
    And Wait to avoid 429 message

  @automation @finished @rainy
  Scenario: Enter non existent uuid
    Given Finalize with eventClose and avoid 429 message
    Given Perform an initSession
    And Wait to avoid 429 message
    And Perform an endSession without uuid
    And Wait to avoid 429 message
    Then Perform an endSession
    And Wait to avoid 429 message

  @automation @finished @happy
  Scenario: Check for different passwords
    Given Finalize with eventClose and avoid 429 message
    Given Perform an initSession
    And Wait to avoid 429 message
    Then Perform an endSession
    And Wait to avoid 429 message
    And Perform an initSession checking username and password
    And Wait to avoid 429 message
    Then Perform an endSession after checking username and password
    And Wait to avoid 429 message

  @automation @finished @happy
  Scenario: Validate same password in the same object
    Given Finalize with eventClose and avoid 429 message
    Given Perform 3 initSession
    And Wait to avoid 429 message
    Then Check 3 username and passwords
    Given Wait to avoid 429 message
    Then Perform 3 endSession
    And Wait to avoid 429 message

  @automation @finished @rainy
  Scenario: Check 429 status after quickly send to the initSession
    Given Finalize with eventClose and avoid 429 message
    Given Perform an initSession
    Then Check 429 message
    Given Wait to avoid 429 message
    Then Perform an endSession
    And Wait to avoid 429 message
