import requests
import time
import unittest
from requests.auth import HTTPBasicAuth

"""Possibilities:
        Ready to test                                     - Camera implementation is OK and test is defined
        Check if asserts are OK. Update to version 0.1.18 - Update firmware version and check the test
        Needs to be implemented in camera                 - Not implemented in camara neither defined over the tests
"""


class internal_Api(unittest.TestCase):
    mac = "AA:BB:CC:DD:EE:53"
    environment = 'dev'
    username = 'mirgor'
    password = 'wayna!'

    def perform_initSession(self):
        print("Performing an initSession")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/initSession',
                            json={'cameraMac': self.mac, 'protocol': 'RTSP'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    def perform_endSession(self, uuid):
        print("Performing an endSession")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/endSession',
                            json={'cameraMac': self.mac, 'uuId': uuid},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    def mac_string_change(self, string_mac):
        separator = '%3A'
        string_mac_sep = string_mac[0:2] + separator + string_mac[3:5] + separator + string_mac[6:8] + separator + \
                         string_mac[9:11] + separator + string_mac[12:14] + separator + string_mac[15:17]
        return string_mac_sep

    def perform_keepAlive(self, uuid):
        print("Performing a keepAlive")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/keepAlive/',
                            json={'cameraMac': self.mac, 'uuId': uuid},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    def perform_aliveMap(self):
        print("Performing an aliveMap")
        mac_changed = self.mac_string_change(self.mac)
        req = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/livestream/private/aliveMap/' +
                           mac_changed,
                           auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    def perform_portTCPInUse(self, port_used):
        print("Performing a portTCPInUse")
        req = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/livestream/private/portTCPInUse/' +
                           str(port_used),
                           auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    @classmethod
    def setUpClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Starting internalApi tests...")
        print("-----------------------------------------")
        print("Precondition: a camera shall be on during this test. Otherwise, every test will fail.")
        print()
        print()

    @unittest.skip("Ready to test")
    def test_initSession_aliveMap(self):
        print("Performing an initSession and an aliveMap")
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId initSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req2 = self.perform_aliveMap()
        resultado2 = req2.json()
        uuId_aliveMap = resultado2[0]['uuId']
        print('uuId aliveMap: ' + uuId_aliveMap)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")
        self.assertEqual(uuId_1, uuId_aliveMap, "both uuId are not the same")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req3 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req3.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_only_aliveMap(self):
        print("Performing an aliveMap without initSession")
        req = self.perform_aliveMap()
        resultado = req.json()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertEqual(0, len(resultado), "There is an initSession launched")

    @unittest.skip("Ready to test")
    def test_aliveMap_mac_characters_check(self):
        print("Performing an aliveMap - MAC with special characters")
        req = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/livestream/private/aliveMap/' +
                           'AA%3ABB%3ACC%3ADD%3AEE%3A5&',
                           auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))
        print()

        # Asserts
        self.assertEqual(400, req.status_code, "status_code was not 400")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        print("Performing an aliveMap - MAC with more than 12 digits")
        req2 = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/livestream/private/aliveMap/' +
                            'AA%3ABB%3ACC%3ADD%3AEE%3A53%3A12',
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))
        print()

        # Asserts
        self.assertEqual(400, req2.status_code, "status_code was not 400")

    @unittest.skip("Ready to test")
    def test_initSession_portTCPInUse(self):
        print("Performing an initSession and a portTCPInUse")
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        port_used = resultado['port']
        print('uuId initSession: ' + uuId_1)
        print('port used: ' + str(port_used))
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req2 = self.perform_portTCPInUse(port_used)
        resultado2 = req2.json()
        port_string = str(port_used)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")
        self.assertTrue(resultado2[port_string], "port is not indicated as used")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req3 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req3.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_portTCPInUse(self):
        print("Performing a portTCPInUse without initSession")
        print('This will be tested over 50199. Please check that there was no camera initiated over this port.')
        req = self.perform_portTCPInUse(50199)
        resultado = req.json()
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")
        self.assertFalse(resultado['50199'], "port is indicated as used")

    @classmethod
    def tearDownClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Tests finished...")
        print("-----------------------------------------")
        print()
        print()


if __name__ == '__main__':
    unittest.main()
