import requests
import time
import unittest
import time
from requests.auth import HTTPBasicAuth

"""Possibilities:
        Ready to test                                     - Camera implementation is OK and test is defined
        Check if asserts are OK. Update to version 0.1.18 - Update firmware version and check the test
        Needs to be implemented in camera                 - Not implemented in camara neither defined over the tests
"""


class eventClose_keepAlive(unittest.TestCase):
    mac = "AA:BB:CC:DD:EE:53"
    environment = 'ist'
    username = 'mirgor'
    password = 'wayna!'

    def perform_initSession(self):
        print("Performing an initSession")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/initSession',
                            json={'cameraMac': self.mac, 'protocol': 'RTSP'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    def perform_endSession(self, uuid):
        print("Performing an endSession")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/endSession',
                            json={'cameraMac': self.mac, 'uuId': uuid},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    def mac_string_change(self, string_mac):
        separator = '%3A'
        string_mac_sep = string_mac[0:2] + separator + string_mac[3:5] + separator + string_mac[6:8] + separator + \
                         string_mac[9:11] + separator + string_mac[12:14] + separator + string_mac[15:17]
        return string_mac_sep

    def perform_eventClose(self):
        print("Performing an eventClose")
        mac_changed = self.mac_string_change(self.mac)
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/eventClose/' +
                            mac_changed,
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    def perform_aliveMap(self):
        print("Performing an aliveMap")
        mac_changed = self.mac_string_change(self.mac)
        req = requests.get('https://' + self.environment + '-vs.mirgor.com.ar/livestream/private/aliveMap/' +
                           mac_changed,
                           auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    def perform_statistics_specs(self):
        print("Performing a statistics/specs")
        req = requests.get('https://' + self.environment +
                           '-vs.mirgor.com.ar/livestream/statistics/specs?page=0&size=100&sort=id,'
                           'desc&search=(cameraMac:%27AA:BB:CC:DD:EE:53%27)',
                           auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))

        return req

    def perform_keepAlive(self, uuid):
        print("Performing a keepAlive")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/keepAlive/',
                            json={'cameraMac': self.mac, 'uuId': uuid},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    @classmethod
    def setUpClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Starting eventClose_keepAlive tests...")
        print("-----------------------------------------")
        print("Precondition: a camera shall be on during this test. Otherwise, every test will fail.")
        print()
        print()

    @unittest.skip("Ready to test")
    def test_eventClose(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req2 = self.perform_eventClose()
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_logout_session(self):
        req = self.perform_eventClose()
        print()

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req2 = self.perform_eventClose()
        print()

        # Asserts
        self.assertEqual(404, req2.status_code, "status_code was not 404")

    @unittest.skip("Ready to test")
    def test_eventClose_mac_characters_check(self):
        print("Performing an eventClose - MAC with special characters")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/eventClose/' +
                            'AA%3ABB%3ACC%3ADD%3AEE%3A5%',
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))
        print()

        # Asserts
        self.assertEqual(400, req.status_code, "status_code was not 400")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        print("Performing an eventClose - MAC with more than 12 digits")
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/eventClose/' +
                             'AA%3ABB%3ACC%3ADD%3AEE%3A53%312',
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))
        print()

        # Asserts
        self.assertEqual(400, req2.status_code, "status_code was not 400")

    @unittest.skip("Ready to test")
    def test_keepAlive(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        # Obtain expiration data
        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req2 = self.perform_aliveMap()
        resultado2 = req2.json()
        time_1 = resultado2[0]['expiration']
        print('expiration: ' + time_1)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req3 = self.perform_keepAlive(uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req3.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        # Obtain new expiration data
        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req4 = self.perform_aliveMap()
        resultado4 = req4.json()
        time_2 = resultado4[0]['expiration']
        print('expiration: ' + time_2)
        print()

        # Asserts
        self.assertEqual(200, req4.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        # Comparing time_1 and time_2
        pattern = '%d-%m-%Y %H:%M:%S'
        date_1 = int(time.mktime(time.strptime(time_1, pattern)))
        date_2 = int(time.mktime(time.strptime(time_2, pattern)))

        self.assertGreater(date_2, date_1, 'After keepAlive, time is not updated.')
        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req5 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req5.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_keepAlive_mac_special_characters(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        # Obtain expiration data
        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req2 = self.perform_aliveMap()
        resultado2 = req2.json()
        time_1 = resultado2[0]['expiration']
        print('expiration: ' + time_1)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        print("Performing a keepAlive")
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/keepAlive/',
                             json={'cameraMac': 'AA:BB:CC:DD:EE:5&', 'uuId': uuId_1},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))
        print()

        # Asserts
        self.assertEqual(400, req3.status_code, "status_code was not 400")

        # --------------------------------------------------------------------------------------------------------------

        # Obtain new expiration data
        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req4 = self.perform_aliveMap()
        resultado4 = req4.json()
        time_2 = resultado4[0]['expiration']
        print('expiration: ' + time_2)
        print()

        # Asserts
        self.assertEqual(200, req4.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req5 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req5.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_keepAlive_mac_more_than_12(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        # Obtain expiration data
        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req2 = self.perform_aliveMap()
        resultado2 = req2.json()
        time_1 = resultado2[0]['expiration']
        print('expiration: ' + time_1)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        print("Performing a keepAlive")
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/keepAlive/',
                             json={'cameraMac': 'AA:BB:CC:DD:EE:53:12', 'uuId': uuId_1},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))
        print()

        # Asserts
        self.assertEqual(400, req3.status_code, "status_code was not 400")

        # --------------------------------------------------------------------------------------------------------------

        # Obtain new expiration data
        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req4 = self.perform_aliveMap()
        resultado4 = req4.json()
        time_2 = resultado4[0]['expiration']
        print('expiration: ' + time_2)
        print()

        # Asserts
        self.assertEqual(200, req4.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req5 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req5.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_keepAlive_non_existent_uuid(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        # Obtain expiration data
        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req2 = self.perform_aliveMap()
        resultado2 = req2.json()
        time_1 = resultado2[0]['expiration']
        print('expiration: ' + time_1)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req3 = self.perform_keepAlive('aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa')   # Wrong uuid
        print()

        # Asserts
        self.assertEqual(400, req3.status_code, "status_code was not 400")

        # --------------------------------------------------------------------------------------------------------------

        # Obtain new expiration data
        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req4 = self.perform_aliveMap()
        resultado4 = req4.json()
        time_2 = resultado4[0]['expiration']
        print('expiration: ' + time_2)
        print()

        # Asserts
        self.assertEqual(200, req4.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req5 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req5.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_keepAlive_with_endSession(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req2 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req3 = self.perform_keepAlive(uuId_1)
        print()

        # Asserts
        self.assertEqual(400, req3.status_code, "status_code was not 400")

    @unittest.skip("Ready to test")
    def test_keepAlive_with_eventClose(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req2 = self.perform_eventClose()
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req3 = self.perform_keepAlive(uuId_1)
        print()

        # Asserts
        self.assertEqual(400, req3.status_code, "status_code was not 400")

    @unittest.skip("Ready to test")
    def test_keepAlive_after_expiration(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        # Obtain if this was expired or not
        expired = False
        while expired is False:
            time.sleep(30)  # Delay to avoid 429 - incorrect answer
            req2 = self.perform_statistics_specs()
            resultado2 = req2.json()
            element_event_close = resultado2['content'][0]['eventClose']
            print('eventClose: ' + str(element_event_close))
            print()

            # Asserts
            self.assertEqual(200, req2.status_code, "status_code was not 200")

            if str(element_event_close) == 'TIMEOUT':
                expired = True

        # --------------------------------------------------------------------------------------------------------------

        # At this point, the session has expired for a TIMEOUT

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req3 = self.perform_keepAlive(uuId_1)
        print()

        # Asserts
        self.assertEqual(400, req3.status_code, "status_code was not 400")

    @classmethod
    def tearDownClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Tests finished...")
        print("-----------------------------------------")
        print()
        print()


if __name__ == '__main__':
    unittest.main()
