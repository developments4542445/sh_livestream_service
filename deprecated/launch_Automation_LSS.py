import unittest
import HtmlTestRunner

# Tests to perform
import VLC_tests
from Automation_Live_Stream.Deprecated import initSession_endSession, internalApi, eventClose_keepAlive

if __name__ == '__main__':
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    suite.addTests(loader.loadTestsFromModule(VLC_tests))
    suite.addTests(loader.loadTestsFromModule(initSession_endSession))
    suite.addTests(loader.loadTestsFromModule(eventClose_keepAlive))
    suite.addTests(loader.loadTestsFromModule(internalApi))

    runner = unittest.TextTestRunner(verbosity=3)
    runner = HtmlTestRunner.\
        HTMLTestRunner(verbosity=3, report_title='Automation Live Stream Service', descriptions='QA Automation',
                       report_name='Automatic Live Stream Service', output='reports', open_in_browser=True)
    results = runner.run(suite)
