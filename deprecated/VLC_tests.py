import sys

import requests
import time
import unittest
import vlc
from requests.auth import HTTPBasicAuth

"""Possibilities:
        Ready to test                                     - Camera implementation is OK and test is defined
        Check if asserts are OK. Update to version 0.1.18 - Update firmware version and check the test
        Needs to be implemented in camera                 - Not implemented in camara neither defined over the tests
"""


class VLC_tests(unittest.TestCase):
    mac = "AA:BB:CC:DD:EE:53"
    environment = 'ist'
    username = 'mirgor'
    password = 'wayna!'

    def perform_initSession(self):
        print("Performing an initSession")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/initSession',
                            json={'cameraMac': self.mac, 'protocol': 'RTSP'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    def perform_endSession(self, uuid):
        print("Performing an endSession")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/endSession',
                            json={'cameraMac': self.mac, 'uuId': uuid},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    @classmethod
    def setUpClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Starting VLC_tests...")
        print("-----------------------------------------")
        print("Precondition: a camera shall be on during this test. Otherwise, every test will fail.")
        print()
        print()

    @unittest.skip("Ready to test")
    def test_initSession_VLC(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        username_1 = resultado['proxyServer']['username']
        password_1 = resultado['proxyServer']['password']
        host_1 = resultado['host']
        port_1 = resultado['port']
        print('username: ' + username_1)
        print('password: ' + password_1)
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        # Play VLC and confirm functionality
        try:
            print('Playing VLC to confirm functionality. Wait 10 seconds to open VLC.')
            mediaplayer = vlc.Instance("--rtsp-tcp")
            player = mediaplayer.media_player_new()
            media = mediaplayer.media_new('rtsp://' + username_1 + ':' + password_1 +
                                          '@' + host_1 + ':' + str(port_1) + '/cam')
            player.set_media(media)
            player.play()
            flag_vlc_error = False
        except:
            print("Error over the VLC streaming.")
            flag_vlc_error = True

        self.assertFalse(flag_vlc_error, "Streaming could not be performed. Error was performed (not from VLC state).")

        # --------------------------------------------------------------------------------------------------------------

        # While video is loading, it shall wait
        player_state = player.get_state()
        while str(player_state) != 'State.Playing':
            player_state = player.get_state()

        time.sleep(2)
        print()
        print("Please, confirm that audio and video are in this streaming."
              + " If not, there is a bug and it shall be loaded in Jira.")
        print("Press enter to finish the streaming and the semi-automatic test.")
        input()

        # --------------------------------------------------------------------------------------------------------------

        req2 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_initSession_Wrong_UserPass(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        username_1 = resultado['proxyServer']['username']
        password_1 = resultado['proxyServer']['password']
        host_1 = resultado['host']
        port_1 = resultado['port']
        print('username: ' + username_1)
        print('password: ' + password_1)
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        # Play VLC and confirm functionality - wrong username
        try:
            print('Playing VLC WITH WRONG USERNAME to confirm functionality.')
            mediaplayer = vlc.Instance("--rtsp-tcp")
            player = mediaplayer.media_player_new()
            media = mediaplayer.media_new('rtsp://' + 'carlitos' + ':' + password_1 +
                                          '@' + host_1 + ':' + str(port_1) + '/cam')
            player.set_media(media)
            player.play()
            flag_vlc_error = False
        except:
            print("Error over the VLC streaming.")
            flag_vlc_error = True

        self.assertFalse(flag_vlc_error, "Streaming could not be performed. Error was performed (not from VLC state).")

        # --------------------------------------------------------------------------------------------------------------

        # Checking if failed or not - User check
        time.sleep(10)
        player_state = player.get_state()   # Get state of the VLC process

        # --------------------------------------------------------------------------------------------------------------

        # endSession if there was a streaming performed
        if str(player_state) == 'State.Playing':
            req2 = self.perform_endSession(uuId_1)
            print('uuId endSession: ' + uuId_1)
            print()

        self.assertEqual('State.Opening', str(player_state),
                         'Streaming success... but it should indicate wrong username.')

        # --------------------------------------------------------------------------------------------------------------

        # At this point, we know that streaming using incorrect username is not performed OK

        # Play VLC and confirm functionality - wrong password
        try:
            print()
            print()
            print('Playing VLC WITH WRONG PASSWORD to confirm functionality.')
            mediaplayer = vlc.Instance("--rtsp-tcp")
            player = mediaplayer.media_player_new()
            media = mediaplayer.media_new('rtsp://' + username_1 + ':' + 'carlos_password' +
                                          '@' + host_1 + ':' + str(port_1) + '/cam')
            player.set_media(media)
            player.play()
            flag_vlc_error = False
        except:
            print("Error over the VLC streaming.")
            flag_vlc_error = True

        self.assertFalse(flag_vlc_error, "Streaming could not be performed. Error was performed (not from VLC state).")

        # --------------------------------------------------------------------------------------------------------------

        # Checking if failed or not - User check
        time.sleep(10)
        player_state2 = player.get_state()  # Get state of the VLC process

        # --------------------------------------------------------------------------------------------------------------

        self.assertEqual('State.Opening', str(player_state2),
                         'Streaming success... but it should indicate wrong password.')

        req2 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

    @classmethod
    def tearDownClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Tests finished...")
        print("-----------------------------------------")
        print()
        print()


if __name__ == '__main__':
    unittest.main()
