import requests
import time
import unittest
from requests.auth import HTTPBasicAuth

"""Possibilities:
        Ready to test                                     - Camera implementation is OK and test is defined
        Check if asserts are OK. Update to version 0.1.18 - Update firmware version and check the test
        Needs to be implemented in camera                 - Not implemented in camara neither defined over the tests
"""


class Live_Stream_Video_initSession_endSession(unittest.TestCase):
    mac = "AA:BB:CC:DD:EE:53"
    environment = 'ist'
    username = 'mirgor'
    password = 'wayna!'

    def perform_initSession(self):
        print("Performing an initSession")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/initSession',
                            json={'cameraMac': self.mac, 'protocol': 'RTSP'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    def perform_endSession(self, uuid):
        print("Performing an endSession")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/endSession',
                            json={'cameraMac': self.mac, 'uuId': uuid},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))

        return req

    @classmethod
    def setUpClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Starting initSession_endSession tests...")
        print("-----------------------------------------")
        print("Precondition: a camera shall be on during this test. Otherwise, every test will fail.")
        print()
        print()

    @unittest.skip("Ready to test")
    def test_initSession_endSession(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req2 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_init_More_Than_One_Session(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req2 = self.perform_initSession()
        resultado2 = req2.json()
        uuId_2 = resultado2['uuId']
        print('uuId: ' + uuId_2)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req3 = self.perform_initSession()
        resultado3 = req3.json()
        uuId_3 = resultado3['uuId']
        print('uuId: ' + uuId_3)
        print()

        # Asserts
        self.assertEqual(200, req3.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req4 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req4.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req5 = self.perform_endSession(uuId_2)
        print('uuId endSession: ' + uuId_2)
        print()

        # Asserts
        self.assertEqual(200, req5.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req6 = self.perform_endSession(uuId_3)
        print('uuId endSession: ' + uuId_3)
        print()

        # Asserts
        self.assertEqual(200, req6.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_init_end_different_1(self):
        # Init More than One Session, End One Session and Init Others Session More
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req2 = self.perform_initSession()
        resultado2 = req2.json()
        uuId_2 = resultado2['uuId']
        print('uuId: ' + uuId_2)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req3 = self.perform_initSession()
        resultado3 = req3.json()
        uuId_3 = resultado3['uuId']
        print('uuId: ' + uuId_3)
        print()

        # Asserts
        self.assertEqual(200, req3.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req4 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req4.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req5 = self.perform_initSession()
        resultado5 = req5.json()
        uuId_5 = resultado5['uuId']
        print('uuId: ' + uuId_5)
        print()

        # Asserts
        self.assertEqual(200, req5.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req6 = self.perform_endSession(uuId_2)
        print('uuId endSession: ' + uuId_2)
        print()

        # Asserts
        self.assertEqual(200, req6.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req7 = self.perform_endSession(uuId_3)
        print('uuId endSession: ' + uuId_3)
        print()

        # Asserts
        self.assertEqual(200, req7.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req8 = self.perform_endSession(uuId_5)
        print('uuId endSession: ' + uuId_5)
        print()

        # Asserts
        self.assertEqual(200, req8.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_init_end_different_2(self):
        # Init More than One Session, End One Session and Init Others Session More
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req2 = self.perform_initSession()
        resultado2 = req2.json()
        uuId_2 = resultado2['uuId']
        print('uuId: ' + uuId_2)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req3 = self.perform_initSession()
        resultado3 = req3.json()
        uuId_3 = resultado3['uuId']
        print('uuId: ' + uuId_3)
        print()

        # Asserts
        self.assertEqual(200, req3.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req4 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req4.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req5 = self.perform_initSession()
        resultado5 = req5.json()
        uuId_5 = resultado5['uuId']
        print('uuId: ' + uuId_5)
        print()

        # Asserts
        self.assertEqual(200, req5.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req6 = self.perform_initSession()
        resultado6 = req6.json()
        uuId_6 = resultado6['uuId']
        print('uuId: ' + uuId_6)
        print()

        # Asserts
        self.assertEqual(200, req6.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req7 = self.perform_initSession()
        resultado7 = req7.json()
        uuId_7 = resultado7['uuId']
        print('uuId: ' + uuId_7)
        print()

        # Asserts
        self.assertEqual(200, req7.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req8 = self.perform_endSession(uuId_2)
        print('uuId endSession: ' + uuId_2)
        print()

        # Asserts
        self.assertEqual(200, req8.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req9 = self.perform_endSession(uuId_3)
        print('uuId endSession: ' + uuId_3)
        print()

        # Asserts
        self.assertEqual(200, req9.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req9 = self.perform_endSession(uuId_5)
        print('uuId endSession: ' + uuId_5)
        print()

        # Asserts
        self.assertEqual(200, req9.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req10 = self.perform_endSession(uuId_6)
        print('uuId endSession: ' + uuId_6)
        print()

        # Asserts
        self.assertEqual(200, req10.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req11 = self.perform_endSession(uuId_7)
        print('uuId endSession: ' + uuId_7)
        print()

        # Asserts
        self.assertEqual(200, req11.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_initSession_empty_field(self):
        print("Performing an initSession without cameraMac")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/initSession',
                            json={'cameraMac': self.mac},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))
        print()

        # Asserts
        self.assertEqual(400, req.status_code, "status_code was not 400")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        print("Performing an initSession without protocol")
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/initSession',
                            json={'protocol': 'RTSP'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))
        print()

        # Asserts
        self.assertEqual(400, req2.status_code, "status_code was not 400")

    @unittest.skip("Ready to test")
    def test_initSession_mac_characters_check(self):
        print("Performing an initSession - MAC with special characters")
        req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/initSession',
                            json={'cameraMac': 'AA:BB:CC:DD:EE:5&', 'protocol': 'RTSP'},
                            auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req.status_code))
        print("Content: " + str(req.content))
        print()

        # Asserts
        self.assertEqual(400, req.status_code, "status_code was not 400")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        print("Performing an initSession - MAC with more than 12 digits")
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/initSession',
                             json={'cameraMac': 'AA:BB:CC:DD:EE:53:12', 'protocol': 'RTSP'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))
        print()

        # Asserts
        self.assertEqual(400, req2.status_code, "status_code was not 400")

    @unittest.skip("Ready to test")
    def test_endSession_mac_characters_check(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        print("Performing an endSession - MAC with special characters")
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/endSession',
                             json={'cameraMac': 'AA:BB:CC:DD:EE:5&', 'uuId': uuId_1},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))
        print()

        # Asserts
        self.assertEqual(400, req2.status_code, "status_code was not 400")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        print("Performing an endSession - MAC with more than 12 digits")
        req3 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/endSession',
                             json={'cameraMac': 'AA:BB:CC:DD:EE:53:12', 'uuId': uuId_1},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req3.status_code))
        print("Content: " + str(req3.content))
        print()

        # Asserts
        self.assertEqual(400, req3.status_code, "status_code was not 400")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req4 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req4.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_endSession_incorrect_uuId(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        print("Performing an endSession - non valid uuId")
        req2 = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/livestream/endSession',
                             json={'cameraMac': self.mac, 'uuId': 'uuId_non_valid'},
                             auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(req2.status_code))
        print("Content: " + str(req2.content))
        print()

        # Asserts
        self.assertEqual(400, req2.status_code, "status_code was not 400")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req4 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req4.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_different_passwords(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        username_1 = resultado['proxyServer']['username']
        password_1 = resultado['proxyServer']['password']
        print('username: ' + username_1)
        print('password: ' + password_1)
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req2 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req3 = self.perform_initSession()
        resultado3 = req3.json()
        uuId_3 = resultado3['uuId']
        username_3 = resultado3['proxyServer']['username']
        password_3 = resultado3['proxyServer']['password']
        print('username: ' + username_3)
        print('password: ' + password_3)
        print('uuId: ' + uuId_3)
        print()

        # Asserts
        self.assertEqual(200, req3.status_code, "status_code was not 200")

        self.assertEqual(username_1, username_3, "username is not the same in both situations")
        self.assertNotEqual(password_1, password_3, "password is the same in both situations")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req4 = self.perform_endSession(uuId_3)
        print('uuId endSession: ' + uuId_3)
        print()

        # Asserts
        self.assertEqual(200, req4.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_same_password_same_object(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        username_1 = resultado['proxyServer']['username']
        password_1 = resultado['proxyServer']['password']
        print('username: ' + username_1)
        print('password: ' + password_1)
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req2 = self.perform_initSession()
        resultado2 = req2.json()
        uuId_2 = resultado2['uuId']
        username_2 = resultado2['proxyServer']['username']
        password_2 = resultado2['proxyServer']['password']
        print('username: ' + username_2)
        print('password: ' + password_2)
        print('uuId: ' + uuId_2)
        print()

        # Asserts
        self.assertEqual(200, req2.status_code, "status_code was not 200")

        self.assertEqual(username_1, username_2, "username is not the same in both situations")
        self.assertEqual(password_1, password_2, "password is not the same in both situations")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req3 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req3.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer

        req4 = self.perform_endSession(uuId_2)
        print('uuId endSession: ' + uuId_2)
        print()

        # Asserts
        self.assertEqual(200, req4.status_code, "status_code was not 200")

    @unittest.skip("Ready to test")
    def test_429_quick_initSession(self):
        req = self.perform_initSession()
        resultado = req.json()
        uuId_1 = resultado['uuId']
        print('uuId: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req.status_code, "status_code was not 200")

        # --------------------------------------------------------------------------------------------------------------

        # Test without delay
        req2 = self.perform_initSession()

        # Asserts
        self.assertEqual(429, req2.status_code, "status_code was not 429")

        # --------------------------------------------------------------------------------------------------------------

        time.sleep(4)  # Delay to avoid 429 - incorrect answer
        req3 = self.perform_endSession(uuId_1)
        print('uuId endSession: ' + uuId_1)
        print()

        # Asserts
        self.assertEqual(200, req3.status_code, "status_code was not 200")

    @classmethod
    def tearDownClass(cls):
        print()
        print()
        print("-----------------------------------------")
        print("Tests finished...")
        print("-----------------------------------------")
        print()
        print()


if __name__ == '__main__':
    unittest.main()
